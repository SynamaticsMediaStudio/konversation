<?php

return [
    "app"=>[
        "name" => "Konversation",
        "version" => "1.0.1",
        "licence" => [
            "key" => "XXXXXX",
            "owner" => "OnlineIAS",
            "email" => "admin@localhost.com",
        ],
    ],
    "system"=>[
        "name"=>"OnlineIAS",
        "theme"=> "red"
    ]
];
