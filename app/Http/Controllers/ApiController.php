<?php

namespace App\Http\Controllers;

use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
class ApiController extends Controller
{

    /**
     * Login API CALL .
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function Login(Request $request)
    {
        $http = new Client;
        try {
            $response = $http->post(route('passport.token'),[
                'form_params' => [
                    'grant_type'        => "password",
                    'client_id'         => config('services.passport.client'),
                    'client_secret'     => config('services.passport.secret'),
                    'username'          => $request->username,
                    'password'          => $request->password
                ]
            ]);
            return $response->getBody();
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $resn = "";
            if($e->getCode() == 400){
                $resn = "Invalid Request. Please Enter a username and/or Password";
            }
            elseif ($e->getCode() == 401) {
                $resn = "Your login details are incorrect. Please try again.";
            }
            else{
                $resn = "Something went wrong on the server. Please try after sometime.";
            }
            return response()->json($resn,$e->getCode());
        }
    }

    /**
     * Register API CALL .
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function Register(Request $request)
    {
        $request->validate([
            'name'      => 'required|string|max:255',
            'email'     => 'required|string|email|max:255|unique:users',
            'password'  => 'required|string|min:6',
        ]);
        return User::create([
            'name' => $request->name,
            'email' => $request->email,
            'color' => $request->color,
            'password' => Hash::make($request->password),
        ]);
    }
    /**
     * Logout API CALL .
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function Logout(Request $request)
    {
       auth()->user()->tokens->each(function($token,$key){
        $token->delete();
       });
       return response()->json("Logged Out",200);
    }
}
