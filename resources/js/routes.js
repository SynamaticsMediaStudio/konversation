
import Home from './components/Marketing/Home';
import Login from './components/Auth/Login';
import Register from './components/Auth/Register';
import Logout from './components/Auth/Logout';
import MyAccount from './components/Account/MyAccount';
import Create from './components/Post/Create';
import PostView from './components/Post/PostView';
import store from './store'
const routes = [
    {path :"/",component:Home,name:"Home"},
    {
        path :"/login",
        component:Login,
        meta: { guest: true },
        name:"Login",
        beforeEnter: (to, from, next) => {
            if (store.token) {
                next('/');
            }
            else{
                next();
            }
        }
    },
    {
        path :"/join",
        component:Register,
        meta: { guest: true },
        name:"Join",
        beforeEnter: (to, from, next) => {
            if (store.token) {
                next('/');
            }
            else{
                next();
            }
        }
    },
    {path :"/logout",component:Logout,name:"Logout",meta: { requiresAuth: true }},
    {path :"/profile",component:MyAccount,name:"MyAccount",meta: { requiresAuth: true }},
    {path :"/create-post",component:Create,name:"Create",meta: { requiresAuth: true }},
    {path :"/post/:id",component:PostView,name:"ViewPost",meta: { }},
];

export default routes;