import Vue from 'vue';
import Vuex from 'vuex';
import NProgress from 'nprogress';
import Axios from 'axios';
window.randomColor = require('randomcolor');

Vue.use(Vuex);
const store = new Vuex.Store({
    state:{
        token:localStorage.getItem('accesstoken')||false,
        user:{},
        pageloader:false,
    },
    getters:{
        loggedIn(state){
            return state.token !== null;
        },
    },
    mutations:{
        getToken(state,token){
            state.token = token;
        },
        destroyToken(state){
            state.token = false;
        }
    },
    actions:{
        Posts(context){
            return new Promise((resolve,reject)=>{
                Axios.get('/api/posts').then(response=>{resolve(response);}).catch(error=>{reject(error)});
            })
        },
        Categories(context){
            return new Promise((resolve,reject)=>{
                Axios.get('/api/categories').then(response=>{resolve(response);}).catch(error=>{reject(error)});
            })
        },
        CreatePost (context, data){
              return new Promise((resolve,reject)=>{
                Axios.post('/api/posts',{
                    title:data.title,
                    content:data.content,
                    tags:data.tags,
                    category:data.category,
                    user:data.user,
                    color:data.color,
                }).then(response=>{
                    new Noty({type: 'success',text: "Created New"}).show();
                    resolve(response);
                })
                .catch(error=>{
                    for (var key in error.response.data.errors) {
                        new Noty({type: 'error',text: error.response.data.errors[key][0]}).show();
                    }
                    reject(error)
                });
            })
        },
        GetPost (context, data){
              return new Promise((resolve,reject)=>{
                Axios.get('/api/posts/'+data.id).then(response=>{
                    resolve(response);
                })
                .catch(error=>{
                    reject(error)
                });
            })
        },
        Auth(context){
            if(context.state.token){
                Axios.defaults.headers.common['Authorization'] = 'Bearer '+context.state.token;
                return new Promise((resolve,reject)=>{
                    Axios.get('/api/user').then(response=>{ context.state.user = response.data;resolve(response);}).catch(error=>{reject(error)});
                }) 
            }
        },
        registerUser(context,data){
            return new Promise((resolve,reject)=>{
                NProgress.start();
                Axios.post('/api/register',{
                    name: data.name,
                    email: data.email,
                    password: data.password,
                    color: randomColor(),
                }).then(response=>{
                    resolve(response);
                }).catch(error=>{
                    for (var key in error.response.data.errors) {
                        new Noty({type: 'error',text: error.response.data.errors[key][0]}).show();
                    }
                    reject(error);
                }).then(res=>{
                    NProgress.done();
                })
            })
        },
        getToken(context,credentials){
            return new Promise((resolve,reject)=>{
                NProgress.start();
                Axios.post('/api/login',{
                    username: credentials.username,
                    password: credentials.password,
                }).then(response=>{
                    const token = response.data.access_token;
                    localStorage.setItem('accesstoken',token);
                    context.commit('getToken',token)
                    resolve(response);
                }).catch(error=>{
                    new Noty({type: 'error',text: error.response.data,}).show();
                    reject(error);
                }).then(res=>{
                    NProgress.done();
                })
            })
        },
        removeToken(context){
            Axios.defaults.headers.common['Authorization'] = 'Bearer '+context.state.token;
            if(context.state.token){
                return new Promise((resolve,reject)=>{
                    Axios.post('/api/logout').then(response=>{
                        localStorage.removeItem('accesstoken');
                        context.commit('destroyToken')
                        resolve(response);
                        new Noty({type: 'success',text: "Logged Out",}).show();
                    }).catch(error=>{
                        new Noty({type: 'error',text: error.response.data,}).show();
                        localStorage.removeItem('accesstoken');
                        context.commit('destroyToken')
                        reject(error);
                    })
                })
            }
        }
    }
});

export default store; 