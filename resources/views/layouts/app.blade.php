<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('settings.system.name', 'Laravel') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel py-1">
            <div class="container">
                <router-link class="navbar-brand" :to="{ name: 'Home' }">{{ config('settings.system.name', 'Laravel') }}</router-link>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav-bar" aria-controls="main-nav-bar" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="main-nav-bar">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                    </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto main-nav">
                        {{-- Common Links --}}
                        <li class="nav-item"><router-link class="nav-link" :to="{ name: 'Home' }">Home</router-link></li>
                        {{-- Guest Links --}}
                        <li v-if="!$store.state.token" class="nav-item"><router-link class="nav-link" :to="{ name: 'Login' }">Login</router-link></li>
                        <li v-if="!$store.state.token" class="nav-item"><router-link class="nav-link" :to="{ name: 'Join' }">Join</router-link></li>
                        {{-- Logged In Links --}}
                        <li v-if="$store.state.token" class="nav-item"><router-link class="nav-link" :to="{ name: 'MyAccount' }"><i class="far fa-user-circle fa-lg"></i></router-link></li>
                        <li v-if="$store.state.token" class="nav-item"><router-link class="nav-link" :to="{ name: 'Logout' }">Logout</router-link></li>
                    </ul>
                </div>
                <form class="form-inline ml-auto my-2 my-lg-0">
                    <input class="form-control mr-sm-2 search-bar" type="search" placeholder="Search" aria-label="Search">
                </form>
            </div>
        </nav>

        <main >
            <main-component></main-component>
        </main>
    </div>
</body>
</html>
